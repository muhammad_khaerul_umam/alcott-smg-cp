import './App.css';
// import { BrowserRouter, Route, Link } from "react-router-dom";
import NavbarComponent from "./components/Navbar"
import Home from "./pages/Home"

function App() {
  return (
    <div className="App">
   <NavbarComponent/>
   <Home/>
    </div>
  );
}

export default App;
