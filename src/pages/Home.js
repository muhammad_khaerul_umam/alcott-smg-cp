import React from 'react'
import Header from "../components/Header"
import OurService from '../components/OurService'


function Home() {
    return (
        <div>
            <Header/>
            <OurService/>
            
        </div>
    )
}

export default Home
