import React, { Component } from 'react'
import Image from 'react-bootstrap/Image'
import Hero from "../components/images/hero.jpg"
import "./Header.css"
import ReactTypingEffect from 'react-typing-effect';
import { Col, Container, Row } from 'react-bootstrap';



export default class Header extends Component {
    render() {
        return (

       <Container>
          <Row>
            <Col > 
            <div className="header"> 
            <Container>
            <div className="typeSection">
         <ReactTypingEffect
        text={["Welcome to Alcott"]}
        cursorRenderer={cursor => <h1><strong>{cursor}</strong></h1>}
        displayTextRenderer={(text, i) => {
          return (
            <h1>
              {text.split('').map((char, i) => {
                const key = `${i}`;
                return (
                  <span
                    key={key}
                    style={i%2 === 0 ? { color: '#499AA9'} : {fontFamily: 'quicksand'}}
                  >{char}</span>
                );
              })}
            </h1>
          );
        }}        
      />
      </div>
      <div className="describeContent">
        <h3>We provide systems that deliver accessible, high quality information and analysis to enable informed decision-making. </h3>
      </div>
      </Container>
     

  
            </div>
            </Col>
            <Col className="imageHeader" >
            <Image src={Hero} fluid></Image></Col>
          </Row>
          </Container>


        )
    }
}
