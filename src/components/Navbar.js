import React from 'react'
import logoAlcott from "../components/images/alcott-navbrand.png"
import {Navbar, Nav, Container, Image} from "react-bootstrap"
import "./Navbar.css"

function NavbarComponent() {
    return (
        <div>
            <Navbar bg="white" variant="light" expand="sm">
  <Container>
    <Navbar.Brand href="#home">
       
      <Image src={logoAlcott}></Image>
    </Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse className="lg" id="basic-navbar-nav">
      <Nav className="me-auto navComp">
       
      </Nav>

      <Nav className="navComp ">
     <a> <Nav.Link href="#home">Home</Nav.Link></a> 
       <a> <Nav.Link href="#about">About</Nav.Link> </a>
       <a> <Nav.Link href="#portfolio">Portfolio</Nav.Link> </a>
       <a> <Nav.Link href="#portfolio">Contact</Nav.Link> </a>
    </Nav>
     
    </Navbar.Collapse>
  </Container>
</Navbar>
{/* <div className="lineHeader">

</div> */}
        </div>
    )
}

export default NavbarComponent
